#!/usr/bin/env python3

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.network.urlrequest import UrlRequest
import requests
import json

class AddLocationForm(BoxLayout):

	temperature = ObjectProperty()
	datetime = ObjectProperty()

	def refresh(self):
		r = requests.get('http://192.168.1.4:8080/') 
		temp = json.loads(r.text)
		self.temperature.text = str(temp['temperature'])
		self.datetime.text = str(temp['datetime'])

class TempDashApp(App):
	pass

if __name__ == '__main__':
	TempDashApp().run()
